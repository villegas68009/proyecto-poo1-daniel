<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioCliente(Request $request)
    {
        $Cliente = Cliente::all();
        //($Cliente);
        return view('Clientes.listado')->with('Cliente', $Cliente);
    }

    public function AgregacionCliente(Request $request)
    {
       $Cliente = Cliente::all();
        return view('Clientes.agregar')->with('Cliente', $Cliente);
        // dd($cliente);
    }

    public function GuardarCliente(Request $request)
    
    {
    
        $this->validate($request, [
            
            'nombre' => 'required', 
        'apellidos' => 'required',
        'cedula'    => 'required',
        'direccion' => 'required',
        'telefono'  => 'required',
        'fecha_de_nacimiento' => 'required',
        'e_mail' => 'required'
        ]);

        $Cliente = new Cliente;
        $Cliente->nombre = $request->nombre;
        $Cliente->apellidos = $request->apellidos;
        $Cliente->cedula = $request->cedula;
        $Cliente->direccion = $request->direccion;
        $Cliente->telefono = $request->telefono;
        $Cliente->fecha_de_nacimiento = $request->fecha_de_nacimiento;
        $Cliente->e_mail = $request->e_mail;
        $Cliente->save();
        return redirect()->route('list.clientes');


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
