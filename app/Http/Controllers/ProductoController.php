<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioProducto(Request $request)
    {
        $producto = Producto::all();
        //dd($producto);
        return view('Productos.inicio')->with('producto', $producto);
    }

    public function CrearProducto(Request $request)
    {
        $producto = Producto::all();
        return view('productos.crear')->with('producto', $producto);

    }

    public function GuardarProducto(Request $request)
    
    {
    
        $this->validate($request, [
            'Nombre' => 'required',
            'Tipo'  => 'required', 
            'Estado'  => 'required',
            'Precio'  => 'required',

        ]);

        $producto = new producto;
        $producto->Nombre = $request->Nombre;
        $producto->Tipo   = $request->Tipo;
        $producto->Estado = $request->Estado;
        $producto->Precio = $request->Precio;
        $producto->save();
        return redirect()->route('list.productos');






    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
