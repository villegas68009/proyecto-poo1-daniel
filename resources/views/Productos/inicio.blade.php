@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modulo de Productos</div>

                <div class="col text-right">
                    <a href="{{ route('crear.productos') }}" class="btn btn-sm btn-primary">Nuevo Producto</a>


                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col"># ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Precio</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($producto as $item)
                          <tr>
                            <th scope="row">{{$item->id}}</th>
                            <td>{{$item->Nombre}}</td>
                            <td>{{$item->Tipo}}</td>
                            <td>{{$item->Estado}}</td>
                            <td>{{$item->Precio}}</td>
                          </tr>

                            @endforeach 
                        </tbody>
                      </table>        



                </div>
            </div>
        </div>
    </div>
</div>
@endsection

