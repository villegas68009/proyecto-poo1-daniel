@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Listado de Clientes</div>

                <div class="col text-right">
                    <a href="{{ route('agreg.clientes') }}" class="btn btn-sm btn-primary">Nuevo Cliente</a>


                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col"># ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Cedula</th>
                            <th scope="col">Direccion</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Fecha de Nacimiento</th>
                            <th scope="col">E mail</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($Cliente as $item)
                          <tr>
                            <th scope="row">{{$item->id}}</th>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->apellidos}}</td>
                            <td>{{$item->cedula}}</td>
                            <td>{{$item->direccion}}</td>
                            <td>{{$item->telefono}}</td>
                            <td>{{$item->fecha_de_nacimiento}}</td>
                            <td>{{$item->e_mail}}</td>
                            
                          </tr>

                            @endforeach 
                        </tbody>
                      </table>        



                </div>
            </div>
        </div>
    </div>
</div>
@endsection

