@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Crear Nuevo Cliente</div>

                <div class="col text-right">
                    <a href="{{ route('list.clientes') }}" class="btn btn-sm btn-success">Cancelar</a>


                  <div class="card-body">

                      <form role="form" method="post" action="{{ route('guard.clientes')}}">
                        {{ csrf_field() }}
                        {{method_field('post') }};
                            
                        
                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombre</label>
                            <input type="text" class="from-control" name="nombre">
                        </div>
                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="apellidos">Apellidos</label>
                            <input type="text" class="from-control" name="apellidos">
                        </div>
                        
                        <div class="col-lg-4">
                            <label class="from-control-label" for="cedula">Cedula</label>
                            <input type="number" class="from-control" name="cedula">
                        </div>    

                        <div class="col-lg-4">
                            <label class="from-control-label" for="direccion">Direccion</label>
                            <input type="text" class="from-control" name="direccion">
                        </div>  

                        <div class="col-lg-4">
                            <label class="from-control-label" for="telefono">Telefono</label>
                            <input type="number" class="from-control" name="telefono">
                        </div>  

                        <div class="col-lg-4">
                            <label class="from-control-label" for="fecha_de_nacimiento">Fecha de Nacimiento</label>
                            <input type="text" class="from-control" name="fecha_de_nacimiento">
                        </div>  

                        <div class="col-lg-4">
                            <label class="from-control-label" for="e_mail">E mail</label>
                            <input type="text" class="from-control" name="e_mail">
                        </div>  

                        
                        
                        <button type="submit" class="btn btn-success pull-right">Guardar</button>

                    </form>
                   



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
